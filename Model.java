import java.util.List;
import java.util.ArrayList;

/**
 * Represents the model of a calculator. 
 * This model stores a list of pressed buttons and 
 * uses the provided algorithm to calculate the result.
 * As well as the infix notation form
 */
 public class Model {

    private Viewer viewer;

    private double result;
    private String infix;
    private List<CalcButton> buttons = new ArrayList<>();
    private ReversePolishNotation algorithm;


    public Model (Viewer viewer) {
        this.viewer = viewer;
        algorithm = new ReversePolishNotation(this);
    }

    /**
     * Accepts a new button input from the user, updates the internal representation
     * of the infix expression, and recalculates the result using the specified algorithm.
     * 
     * @param button The calculator button pressed by the user.
     */
    public void addUserInput(CalcButton button){
        buttons.add(button);
        infix = generateInfix();
        result = algorithm.calc(infix);
    }
    
    /**
     * Updates the internal representation of the infix expression based on the sequence
     * of button inputs.
     * 
     * @return An infix expression generated from the user's button input.
     */
    private String generateInfix() {
        // this.infix = buttons.stream...
		StringBuilder infixBuilder = new StringBuilder();
		for (CalcButton button : buttons) {
			infixBuilder.append(button.getLabel());
		}
		String infix = infixBuilder.toString();
		return infix;

    }
    
}
