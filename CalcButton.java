import java.util.Optional;
import java.util.Arrays;


/**
 * Represents the various buttons available on a calculator.
 * Each enum constant corresponds to a specific button on the calculator and
 * has an associated label that can be displayed on the button.
 */
 public enum CalcButton {

    ZERO("0"),
    ONE("1"),
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    ADD("+"),
    SUBTRACT("-"),
    MULTIPLY("x"),
    DIVIDE("/"),
    EQUAL("="),
    POINT("."),
    PERCENTAGE("%"),
    CLEAR("C"),
    BACKSPACE("B"),
    OPEN_PARENTHESIS("("),
    CLOSE_PARENTHESIS(")");

    /** The label associated with this calculator button. */
    private final String label;

    CalcButton(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
    
    public static Optional<CalcButton> getByLabel(String label) {
        return Arrays.stream(CalcButton.values())
            .filter(btn -> btn.label.equals(label))
            .findFirst();
    }

}
