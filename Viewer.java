import javafx.application.Application;
import javafx.scene.control.Label;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import java.io.IOException;

// import javafx.scene.control.Button;
// import java.util.Optional;
/**
 * The class implements the Viewer interface for a calculator application
 * using JavaFX as the GUI framework.
 */
public class Viewer extends Application {

    @FXML
    private Label expressionLabel;
    @FXML
    private Label resultLabel;
    
    private static final String FXML_RESOURCE = "/resources/UI.fxml";
    private static final String CSS_RESOURCE = "/resources/style.css";
    private static final String ICON_RESOURCE = "/resources/icons/icon.png";

    private Controller controller;

    public Viewer() {
        controller = new Controller(this);
    }

    /**
     * Creates and displays a stage from FXML file and an Application icon.
     * It also sets the current Viewer instance as a controller for JavaFX.
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_RESOURCE));
        loader.setControllerFactory( clazz -> controller);
        Parent root = loader.load();
        // Font customFont = Font.loadFont(getClass().getResourceAsStream(FONT_RESOURCE_GILROY), 24);

        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource(CSS_RESOURCE).toExternalForm());
        stage.getIcons().add(new Image(ICON_RESOURCE));
        stage.setScene(scene);
        stage.show();
    }

    /**
    * Updates and displays the result and infix expression
    * in the associated labels from the model.
    */
    public void displayResult(String infix, double result) {
        expressionLabel.setText(infix);

        // TODO: need format output
        resultLabel.setText(String.valueOf(result));
    }

}
