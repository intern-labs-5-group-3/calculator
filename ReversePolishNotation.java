/**
 * The class implements the Reverse Polish Notation (RPN) algorithm.
 */
public class ReversePolishNotation{
    private Model model;

    public ReversePolishNotation (Model model){
        this.model = model;
    }

    /** 
     * Calculates the result based on the provided infix expression.
     *
     * @param infix The infix expression to be calculated.
     * @return The result of the calculation.
     */
    public double calc(String infix) {
        return 42;
    }

}
