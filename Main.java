import javafx.application.Application;
/**
 * Represents the entry point for a calculator application using JavaFX for its GUI.
 * This class initializes the MVC components: Model, View (represented by the Viewer interface),
 * and Controller.
 */
public class Main {

    public static void main(String[] args) {
        Viewer viewer = new Viewer();
        Application.launch(Viewer.class, args);
    } 

}
