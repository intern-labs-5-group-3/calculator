import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import java.util.Optional;
import javafx.scene.control.Button;
/**
 * Represents the controller in the MVC architecture of a calculator.
 * This controller listens to user interactions (like key presses) and
 * directs them to the model for appropriate processing.
 */
public class Controller implements EventHandler<ActionEvent> {
     
    private Model model;

    public Controller (Viewer viewer) {
        model = new Model(viewer);
    }

    /**
     * Handles the event of a calculator button being pressed.
     * It passes the input to the model for further processing.
     * 
     * @param button The calculator button that was pressed by the user.
     */

     /**
     * Handles button clicks caused by user interaction with the JavaFX UI.
     *
     * Although this method is a part of the Viewer class,
     * it essentially acts as a controller for handling
     * the interactions in a JavaFX application.
     *
     * This method should be associated with the user interface elements
     * defined in the FXML file to appropriately handle user interactions.
     *
     * The method receives the text value of the pressed button,
     * converts it into an enumeration and sends it to the MVS controller.
     * Text labels in Viewer are also updated.
     *
     * @param event the action event initiated by the user interaction
     */
    // @FXML
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getSource();
        String label = button.getText();
        Optional<CalcButton> btn = CalcButton.getByLabel(label);
        model.addUserInput(btn.get());
    }

}
